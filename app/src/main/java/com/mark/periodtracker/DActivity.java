package com.mark.periodtracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;


public class DActivity extends Activity {
    private  boolean isRoll =  false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_d);
        init();
    }
    public  void  cmd_back_clicked(View view){
        back();
    }
    public  void  cmd_clicked(View view){

        if(!isRoll){
            variables.daysLong  ="3";
        }
        Log.d("val",variables.daysLong+"");
        startActivity(new Intent(getApplicationContext(),EActivity.class));
        overridePendingTransition(0,0);
        finish();

    }

    private void init() {

        android.widget.NumberPicker np = (android.widget.NumberPicker) findViewById(R.id.picker_period);
        np.setMinValue(3);
        np.setMaxValue(7);
        np.setOnValueChangedListener(new android.widget.NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(android.widget.NumberPicker picker, int oldVal, int newVal) {
                // TODO Auto-generated method stub
                Log.d("val",newVal+"");
                isRoll = true;
                variables.daysLong =  (newVal)+"";

            }
        });


        if(variables.isFromMain){
            variables.sharedPref = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
            String daysLong =  variables.sharedPref.getString("daysLong","null");
            np.setValue(Integer.valueOf(daysLong));
        }

    }
    private  void  back(){
        startActivity(new Intent(getApplicationContext(),CActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }
}
