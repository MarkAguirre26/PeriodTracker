package com.mark.periodtracker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.Calendar;

public class PredictionActivity extends Activity {
    private static final String TAG = "MainActivity";
    TextView txt_gender,txt_mbday,txt_fbday;
    EditText txt_conception;
    int yearF,monthF,dayF,yearM,monthM,dayM;

    private DatePickerDialog.OnDateSetListener mDateSetListener_f;
    private DatePickerDialog.OnDateSetListener mDateSetListener_m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prediction);

        init();
    }
    public void  cmd_f(View v){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener_f,year,month,day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }
    public void  cmd_m(View v){
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_MinWidth,mDateSetListener_m,year,month,day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();

    }
    private void init() {
        txt_mbday = (TextView) findViewById(R.id.txt_mbday);
        txt_fbday = (TextView) findViewById(R.id.txt_fbday);
        txt_conception = (EditText) findViewById(R.id.txt_conception);
        txt_gender = (TextView)findViewById(R.id.txt_gender);
        mDateSetListener_f = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);
                String date = month + "/" + day + "/" + year;
                txt_fbday.setText("Father Birthday: "+date);
                yearF = Integer.valueOf(String.valueOf(year).substring(2));
                monthF = month;
                dayF = day;

            }
        };
        mDateSetListener_m = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);
                String date = month + "/" + day + "/" + year;
                txt_mbday.setText("Mother Birthday: "+date);
                yearM = Integer.valueOf(String.valueOf(year).substring(2));
                monthM= month;
                dayM = day;
            }
        };
    }

    public void  cmd_calculate(View view){
        try {
            int conception =Integer.valueOf(txt_conception.getText().toString());
            int valf = (yearF+monthF+dayF)+(yearM+monthM+dayM)+(conception)-19;
            if((valf % 2)==0){
                txt_gender.setText("GIRL");
            }else{
                txt_gender.setText("BOY");
            }
        }catch (Exception e){
            txt_gender.setText("Invalid");
        }



    }

    public  void cmd_back_clicked(View v){
       finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
