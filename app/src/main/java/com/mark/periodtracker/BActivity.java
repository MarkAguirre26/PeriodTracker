package com.mark.periodtracker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v13.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.HorizontalCalendarListener;


public class BActivity extends Activity {
    String   dateNow;
    TextView txt_days,txt_month;
    private HorizontalCalendar horizontalCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        txt_days = (TextView)findViewById(R.id.txt_days) ;
        txt_month = (TextView)findViewById(R.id.txt_month);

//        setDate();


        /** end after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        /** start before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);


        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.add(Calendar.MONTH, -1);
        defaultDate.add(Calendar.DAY_OF_WEEK, +5);

        horizontalCalendar = new HorizontalCalendar.Builder(this, R.id.calendarView)
                .startDate(startDate.getTime())
                .endDate(endDate.getTime())
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .showDayName(true)
                .showMonthName(true)
                .defaultSelectedDate(defaultDate.getTime())
                .textColor(Color.LTGRAY, Color.WHITE)
                .selectedDateBackground(Color.TRANSPARENT)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {

                variables.lastperiod  = DateFormat.getDateInstance().format(date);
//                Log.d("ddd",DateFormat.getDateInstance().format(date));
                setDate(variables.lastperiod);
            }

        });

    }

    private void setDate(String date) {
//        if(variables.isFromMain){
//            variables.sharedPref = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
//            String lastperiod =  variables.sharedPref.getString("lastperiod","null");
//            dateNow =  lastperiod;
//            Log.d("dNowFromMain",dateNow);
//        }else{
//            dateNow = date;
//            Log.d("dNow",dateNow);
//        }

        txt_month.setText(date.substring(0,(date.length()-8)));
        txt_days.setText(date.substring((date.length()-8),(date.length()-6)));
    }




    public  void  cmd_clicked(View view){

        startActivity(new Intent(getApplicationContext(),CActivity.class));
        overridePendingTransition(0,0);
        finish();


    }
    public  void  cmd_back_clicked(View view){
        back();
    }
    private  void  back(){
        if(variables.isFromMain== true){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }else{
            startActivity(new Intent(getApplicationContext(),AActivity.class));
        }

        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }


}
