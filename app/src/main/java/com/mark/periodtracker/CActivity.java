package com.mark.periodtracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;

import android.widget.NumberPicker;
import android.widget.Toast;


import java.util.Date;

import io.blackbox_vision.wheelview.view.WheelView;


public class CActivity extends Activity {
    boolean isRoll =  false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_c);
        init();
    }

    private void init() {



        NumberPicker np = (NumberPicker) findViewById(R.id.picker_days);
        np.setMinValue(25);
        np.setMaxValue(35);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                // TODO Auto-generated method stub
                isRoll = true;
                Log.d("val",newVal+"");
                variables.cycleLong = (newVal)+"";
            }
        });


        if(variables.isFromMain){
            variables.sharedPref = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
            String cycleLong =  variables.sharedPref.getString("cycleLong","null");
            np.setValue(Integer.valueOf(cycleLong));
        }

    }

    public  void  cmd_back_clicked(View view){
        back();
    }
    public  void  cmd_clicked(View view){
        if(!isRoll){
            variables.cycleLong = "25";
        }
        Log.d("val",variables.cycleLong+"");
        startActivity(new Intent(getApplicationContext(),DActivity.class));
        overridePendingTransition(0,0);
        finish();


    }

    private  void  back(){
        startActivity(new Intent(getApplicationContext(),BActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }
}
