package com.mark.periodtracker;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;

import java.text.SimpleDateFormat;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {
    TextView txt_mos, txt_rem_days;
    private String TAG = "result";
    LinearLayout l_ovulation;
    private static final int MY_PERMISSIONS_REQUEST_SEND_SMS = 0;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");


        txt_mos = (TextView) findViewById(R.id.txt_mos);
        txt_rem_days = (TextView) findViewById(R.id.txt_rem_days);


//        if (isNetworkAvailable()) {
//
//            progressDialog.show();
//
//           new Thread(new Runnable() {
//               @Override
//               public void run() {
//                   StringRequest stringRequest = new StringRequest(Request.Method.GET, urls.base_url,
//                           new Response.Listener<String>() {
//                               @Override
//                               public void onResponse(String response) {
//                                   Log.d("result", response + "");
//                                   if (response.contains("Connected")) {
//
//                                   } else {
//                                       finish();
//                                       Toast.makeText(getApplicationContext(), "App deactivated by the developer", Toast.LENGTH_LONG).show();
//                                   }
//
//                               }
//                           }, new Response.ErrorListener() {
//                       @Override
//                       public void onErrorResponse(VolleyError error) {
//                           Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
//                       }
//                   });
//                   MySingleton.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
//               }
//           }).start();
        init();
        progressDialog.dismiss();

//        } else {
//            finish();
//            Toast.makeText(this, "No Internet", Toast.LENGTH_SHORT).show();
//        }

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void init() {
TextView    txt_daysLabel = (TextView)findViewById(R.id.txt_daysLabel);
//        txt_def = (TextView) findViewById(R.id.txt_def);
//        txt_rem_days_ovulation = (TextView) findViewById(R.id.txt_rem_days_ovulation);
//        txt_def_ovulation = (TextView) findViewById(R.id.txt_def_ovulation);
//        l_ovulation = (LinearLayout)findViewById(R.id.l_ovulation);

//        new Thread(new Runnable() {
//            @Override
//            public void run() {


                tools.getValues(getApplicationContext());

                final CompactCalendarView compactCalendar = (CompactCalendarView) findViewById(R.id.compactcalendar_view);
                compactCalendar.setFirstDayOfWeek(Calendar.MONDAY);
                Timestamp ts_sub_ovu = null;
                Timestamp ts_default = null;
                Timestamp ts = null;
                Timestamp ts_ovulation = null;
                Timestamp ts_sub = null;
                Date d = stringToDate(variables.lastperiod, "MMM dd, yyyy");
                Date d_sub = d;

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat month_date = new SimpleDateFormat("m");
                int currentMonth = Integer.valueOf(month_date.format(cal.getTime()));
                int lastPeriodMont = Integer.valueOf(month_date.format(d.getTime()));
                int resMonth = currentMonth - lastPeriodMont;

                setMonth(new Date());

                Log.d("perioddd1", variables.lastperiod + " " + variables.cycleLong + " " + variables.daysLong);

                long days_period = getDifferenceDays(new Date(), addDay(d, Integer.valueOf(variables.cycleLong)));
//        long days_ovulation = getDifferenceDays(new Date(),setOvulation(addDay(d,Integer.valueOf(variables.cycleLong))));
                Log.d("perioddd2", resMonth + " " + currentMonth + " " + lastPeriodMont + " " + days_period);


                txt_rem_days.setText((days_period + 1) + "");
                if ((days_period + 1) <= 5) {//if days is <= 5 days then show notification
                    if ((days_period + 1) == 1) {
                        Notify((days_period + 1) + " day before your period");
                        txt_daysLabel.setText("day");
                    }else{
                        Notify((days_period + 1) + " days before your period");
                        txt_daysLabel.setText("days");
                    }

                }


//        txt_rem_days_ovulation.setText((days_ovulation+resMonth)+"");
//
//        if(days_period >1){
//            txt_def.setText("Days before period");
//        }else{
//            txt_def.setText("Period day");
//        }
//
//        if(days_ovulation <=0){
//            l_ovulation.setVisibility(View.GONE);
//        }
//
//        if(days_ovulation >1){
//            txt_def_ovulation.setText("Days before ovulation");
//        }else{
//            txt_def_ovulation.setText("Ovulation day");
//        }

                ts_default = new java.sql.Timestamp(d.getTime());
                long e_default = ts_default.getTime();
                Event evt_default = new Event(Color.RED, e_default, "Period");
                compactCalendar.addEvent(evt_default, true);


                for (int s = 1; s < Integer.valueOf(variables.daysLong); s++) {
                    d_sub = addDay(d, s);
                    ts_default = new java.sql.Timestamp(d_sub.getTime());
                    long es_default = ts_default.getTime();
                    Event evts_default = new Event(Color.RED, es_default, "Period");
                    compactCalendar.addEvent(evts_default, true);
                }


                for (int i = 0; i < 365 / Integer.valueOf(variables.cycleLong); i++) {

                    d = addDay(d, Integer.valueOf(variables.cycleLong));

                    Date dateOvulation = setOvulation(d);
                    ts_ovulation = new java.sql.Timestamp(dateOvulation.getTime());
                    long ovulation_day = ts_ovulation.getTime();
                    Event ent_ovulation = new Event(Color.BLUE, ovulation_day, "Fertile Day");

                    ts = new java.sql.Timestamp(d.getTime());
                    long period_days = ts.getTime();
                    Event ent = new Event(Color.RED, period_days, "Period");

                    if (i < 1) {


                        Calendar calsms = Calendar.getInstance();
                        calsms.setTime(d);
                        SimpleDateFormat month_datesms = new SimpleDateFormat("MMMM dd, yyyy");
                        Date ovuDate = addDay(dateOvulation, 5);
                        String periodDate = month_datesms.format(d.getTime());
                        String FertileDate = month_datesms.format(dateOvulation.getTime());
                        String OvulationDate = month_datesms.format(ovuDate.getTime());

                        sendSMSMessage("Period: " + periodDate + "\nFertile: " + FertileDate + "\nOvulation: " + OvulationDate);
                    }

                    compactCalendar.addEvent(ent_ovulation, true);
                    compactCalendar.addEvent(ent, true);

                    for (int s = 1; s <= 5; s++) {
                        Date d_ovulation_sub = null;
                        d_ovulation_sub = addDay(dateOvulation, s);
                        ts_sub_ovu = new java.sql.Timestamp(d_ovulation_sub.getTime());
                        long es_default = ts_sub_ovu.getTime();
                        Event evts_default = null;
                        if (s <= 4) {
                            evts_default = new Event(Color.BLUE, es_default, "Fertile Day");
                        } else if (s == 5) {
                            evts_default = new Event(Color.GREEN, es_default, "Ovulation Day");
                        }
                        compactCalendar.addEvent(evts_default, true);
                    }


                    for (int s = 1; s < Integer.valueOf(variables.daysLong); s++) {
                        d_sub = addDay(d, s);
                        ts_default = new java.sql.Timestamp(d_sub.getTime());
                        long es_default = ts_default.getTime();
                        Event evts_default = new Event(Color.RED, es_default, "Period");
                        compactCalendar.addEvent(evts_default, true);
                    }


                }


                compactCalendar.setListener(new CompactCalendarView.CompactCalendarViewListener() {
                    @Override
                    public void onDayClick(Date dateClicked) {
                        List<Event> events = compactCalendar.getEvents(dateClicked);
                        //  Log.d(TAG, "Day was clicked: " + dateClicked + " with events " + events.toString());
                        String event = null;
                        for (Event e : events) {
                            event = String.valueOf(e.getData());

                        }
                        if (event != null) {
                            new SweetAlertDialog(MainActivity.this)
                                    .setTitleText(event)
                                    .show();
                        }

                    }

                    @Override
                    public void onMonthScroll(Date firstDayOfNewMonth) {
                        setMonth(firstDayOfNewMonth);

                    }
                });



//            }
//        }).start();


        //send sms
//        sendSMSMessage("Period in "+days_period+" days "+addDay(d,Integer.valueOf(variables.cycleLong))+"\nOvulation in "+days_ovulation+" days "+setOvulation(addDay(d,Integer.valueOf(variables.cycleLong))));
//        Log.d("sms","Period in "+days_period+" days -"+addDay(d,Integer.valueOf(variables.cycleLong))+"\nOvulation in "+days_ovulation+" days -"+ setOvulation(addDay(d,Integer.valueOf(variables.cycleLong))));
    }

    private void Notify(String msg) {
        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(msg).setSmallIcon(R.drawable.venus)
                .setContentIntent(pIntent).build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);

    }

    private Date setOvulation(Date d) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.DATE, -19);
        Date dateOvulation = cal.getTime();
        return dateOvulation;
    }

    private void setMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        txt_mos.setText(tools.getMonth(month));
    }
//    public void cmd_edit_period(View view){
//      editDate();
//    }

    private void editDate() {
        variables.isFromMain = true;
        startActivity(new Intent(getApplicationContext(), BActivity.class));
        overridePendingTransition(0, 0);
        finish();
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    private Date stringToDate(String aDate, String format) {
        String mytime = aDate;
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);//
        Date myDate = null;
        try {
            myDate = dateFormat.parse(mytime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return myDate;
    }

    public static Date addDay(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, i);
        return cal.getTime();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_prediction:
                startActivity(new Intent(getApplicationContext(), PredictionActivity.class));
                overridePendingTransition(0, 0);
//                finish();
                break;
            case R.id.action_edit:
                editDate();
                break;
            case R.id.action_phonenumber:
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                final EditText edittext = new EditText(this);
                alert.setMessage("Enter partner cellphone number");
                alert.setTitle("Phone Number");
                edittext.setText(variables.partnerNumber);
                alert.setView(edittext);
                alert.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        variables.partnerNumber = edittext.getText().toString();

                        dialog.dismiss();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                alert.show();
                break;
            default:
                break;

        }
        return true;
    }

    protected void sendSMSMessage(String message) {
        variables.message = message;
        if (variables.partnerNumber.length() >= 11) {
            try {
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage(variables.partnerNumber, null, variables.message, null, null);
//            Toast.makeText(getApplicationContext(), "SMS Sent!",Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(),
                        "SMS faild, please try again later!",
                        Toast.LENGTH_LONG).show();
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure?")
                .setContentText("Application Exit!")
                .setConfirmText("YES")
                .setCancelText("No")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        finish();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                    }
                })
                .show();
    }
}
