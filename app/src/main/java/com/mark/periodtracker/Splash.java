package com.mark.periodtracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

public class Splash extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        tools.setFullScreen(this);
        SharedPreferences sharedP =  this.getPreferences(Context.MODE_PRIVATE);
        final String lastperiod = sharedP.getString(variables.lastperiod,"none");



        try{
            new CountDownTimer(3000, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {




                    tools.getValues(getApplicationContext());

                    if(variables.lastperiod.contentEquals("null")){
                        startActivity(new Intent(getApplicationContext(),AActivity.class));
                    }else{
                        startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    }
                    overridePendingTransition(0,0);
                    finish();
                }
            }.start();
        }catch (Exception e){
            Log.d("err",e.getMessage().toString());
        }
    }

}
