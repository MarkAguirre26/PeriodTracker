package com.mark.periodtracker;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.*;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class EActivity extends Activity {
//    TextView txt_bday;
boolean isRoll =  false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_e);
        init();
    }
    public  void  cmd_back_clicked(View view){
        back();
    }
    public  void  cmd_clicked(View view){
        if(!isRoll){
            variables.birthDay = "1970";
        }
        startActivity(new Intent(getApplicationContext(),FActivity.class));
        overridePendingTransition(0,0);
        finish();

    }
    private void init() {

//        txt_bday= (TextView)findViewById(R.id.txt_bday);
//        DateFormat dateFormat;
//        dateFormat = DateFormat.getDateInstance(DateFormat.LONG);
//        String   strDate = dateFormat.format(new Date().getTime());


        android.widget.NumberPicker np = (android.widget.NumberPicker) findViewById(R.id.picker_days);
        np.setMinValue(1970);
        np.setMaxValue(2017);
        np.setOnValueChangedListener(new android.widget.NumberPicker.OnValueChangeListener() {

            @Override
            public void onValueChange(android.widget.NumberPicker picker, int oldVal, int newVal) {
                // TODO Auto-generated method stub
                Log.d("val",newVal+"");
                isRoll = true;
                variables.birthDay= (newVal+1)+"";
            }
        });


//
//        if(variables.isFromMain){
//            variables.sharedPref = getSharedPreferences("myPreference", Context.MODE_PRIVATE);
//            String birthDay =  variables.sharedPref.getString("birthDay","null");
//            strDate = birthDay;
//        }
//        txt_bday.setText(strDate);
    }
//    public  void  cmd_edit_bday(View view){
//        Calendar calendar = Calendar.getInstance();
//        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DatePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                DateFormat dateFormat;
//                dateFormat = DateFormat.getDateInstance(DateFormat.LONG);
//                String   strDate = dateFormat.format(newDate.getTime());
//                Log.d("setDate", String.valueOf(strDate));
//                txt_bday.setText(strDate);
//
//            }
//
//        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.show();
//    }

    private  void  back(){
        startActivity(new Intent(getApplicationContext(),DActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }
}
