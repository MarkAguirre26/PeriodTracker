package com.mark.periodtracker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class FActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_f);

    }
    public  void  cmd_back_clicked(View view){
        back();
    }


    public  void  cmd_save_Clicked(View view){

        variables.sharedPref =this.getSharedPreferences("myPreference",Context.MODE_PRIVATE);
        SharedPreferences.Editor e = variables.sharedPref.edit();
        e.putString("lastperiod",variables.lastperiod);
        e.putString("cycleLong",variables.cycleLong);
        e.putString("daysLong",variables.daysLong);
        e.putString("birthDay",variables.birthDay);
        e.commit();

      startActivity(new Intent(getApplicationContext(),MainActivity.class));
      overridePendingTransition(0,0);
      finish();
    }

    private  void  back(){
        startActivity(new Intent(getApplicationContext(),EActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        back();
    }
}
